# Single mode: basic example

These notebooks can be run in the cloud using Binder:

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/esa%2Fpyxel-data/HEAD?urlpath=lab/tree/examples/single/single.ipynb)
